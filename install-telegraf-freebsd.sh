#!/bin/sh

pkg update
pkg upgrade --yes
pkg install --yes telegraf


# service -h to see available option and rc config file location
# system/rc file location /usr/local/etc/rc.d/telegraf 

# Add the following lines to /etc/rc.conf to enable telegrafb:
# use command sysrc to change config value

# telegraf_enable="YES"
#
# telegraf_enable (bool):       Set to YES to enable telegraf
#                               Default: NO
# telegraf_conf (str):          telegraf configuration file
#                               Default: ${PREFIX}/etc/telegraf.conf
# telegraf_confdir (str):       telegraf configuration directory
#                               Default: none
# telegraf_user (str):          telegraf daemon user
#                               Default: telegraf
# telegraf_group (str):         telegraf daemon group
#                               Default: telegraf
# telegraf_flags (str):         Extra flags passed to telegraf
#                               Default: --quiet

# write to file /etc/rc.conf -> telegraf_enable="yes"
# enable telegraf on system booting
sysrc telegraf_enable=yes

# set config using URL
sysrc telegraf_conf=http://localhost/config

# start telegraf
service telegraf start

# stop telegraf
service telegraf stop

#reference
# https://kifarunix.com/install-and-configure-telegraf-on-freebsd-12/