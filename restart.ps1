function IsServiceExist {
    #check service exist
    $service = Get-Service -Name W32Time -ErrorAction SilentlyContinue
    return ($service.Length -gt 0) 
}

function RestartTelegrafService{
    write-host "Restarting agent..."

    $ServiceName = 'telegraf'
    Restart-Service $ServiceName
    $arrService = Get-Service -Name $ServiceName
    
    if ($arrService.Status -eq 'Running')
    {
        Write-Host 'Success restart agent!' -ForegroundColor green
    }else{
        Write-Host 'Failed to restart agent!' -ForegroundColor red -BackgroundColor white
    }
}

# Full path of the file
$file = 'C:\Program Files\InfluxData\telegraf\telegraf.exe'
#check file exist and service already registered
if ((Test-Path -Path $file -PathType Leaf) -and (IsServiceExist) ) {
    RestartTelegrafService
}else {
    #Telegraf not exist
    Write-Host "The agent is not installed yet" -ForegroundColor red -BackgroundColor white
}