#!/bin/sh

# The [ -t 1 ] check only works when the function is not called from
# a subshell (like in `$(...)` or `(...)`, so this hack redefines the
# function at the top level to always return false when stdout is not
# a tty.
if [ -t 1 ]; then
  is_tty() {
    true
  }
else
  is_tty() {
    false
  }
fi

setup_color() {
  # Only use colors if connected to a terminal
  if is_tty; then
    RAINBOW="
      $(printf '\033[38;5;196m')
      $(printf '\033[38;5;202m')
      $(printf '\033[38;5;226m')
      $(printf '\033[38;5;082m')
      $(printf '\033[38;5;021m')
      $(printf '\033[38;5;093m')
      $(printf '\033[38;5;163m')
    "
    RED=$(printf '\033[31m')
    GREEN=$(printf '\033[32m')
    YELLOW=$(printf '\033[33m')
    BLUE=$(printf '\033[34m')
    BOLD=$(printf '\033[1m')
    RESET=$(printf '\033[m')
  else
    RAINBOW=""
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    RESET=""
  fi
}

printf_netmonk() {
    printf '%s _________%s_____%s  /%s_______ ___%s______%s__________%s  /__%s\n' $RAINBOW $RESET
    printf '%s __  __ \%s _ \ %s __/%s_  __ `__ \%s  __ \%s_  __ \_  %s//_/%s\n' $RAINBOW $RESET
    printf '%s _  / / %s  __/%s /_ _%s   / / / / /%s /_/ /%s  / / / %s ,<   %s\n' $RAINBOW $RESET
    printf '%s /_/ /_/%s\___/%s\__/%s /_/ /_/ /_/%s\____/%s/_/ /_/%s/_/|_|  %s....is now installed!%s\n' $RAINBOW $GREEN $RESET

}

setup_color
printf_netmonk