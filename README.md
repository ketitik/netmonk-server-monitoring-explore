# Telegraf
shell script to install telegraf agent

# How To Run

Untuk Windows Server, jalankan perintah ini di powerShell:

```ps1
$ScriptFromGithHub = Invoke-WebRequest https://bitbucket.org/ketitik/netmonk-server-monitoring-explore/raw/f15645e344e6c71c60aed7783f982a0a61868389/install.ps1; Invoke-Expression $($ScriptFromGithHub.Content)

```

Untuk Linux Server

```sh
bash -c "$(curl -fsSL https://bitbucket.org/ketitik/netmonk-server-monitoring-explore/raw/f15645e344e6c71c60aed7783f982a0a61868389/install.sh)"
```