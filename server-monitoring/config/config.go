package config

// YAMLConfigLoader is the loader of YAML file configuration.
type YAMLConfigLoader struct {
	fileLocation string
}

const (
	statusNormal   = "normal"
	statusWarning  = "warning"
	statusCritical = "critical"

	linkStatusBound                     = "link"
	defaultNormalStatusLowerBoundLink   = 0
	defaultNormalStatusUpperBoundLink   = 50
	defaultWarningStatusLowerBoundLink  = 50
	defaultWarningStatusUpperBoundLink  = 80
	defaultCriticalStatusLowerBoundLink = 80
	defaultCriticalStatusUpperBoundLink = 100

	cpuStatusBound                     = "cpu"
	defaultNormalStatusLowerBoundCPU   = 0
	defaultNormalStatusUpperBoundCPU   = 50
	defaultWarningStatusLowerBoundCPU  = 50
	defaultWarningStatusUpperBoundCPU  = 80
	defaultCriticalStatusLowerBoundCPU = 80
	defaultCriticalStatusUpperBoundCPU = 100

	memoryStatusBound                     = "memory"
	defaultNormalStatusLowerBoundMemory   = 0
	defaultNormalStatusUpperBoundMemory   = 50
	defaultWarningStatusLowerBoundMemory  = 50
	defaultWarningStatusUpperBoundMemory  = 80
	defaultCriticalStatusLowerBoundMemory = 80
	defaultCriticalStatusUpperBoundMemory = 100

	storageStatusBound                     = "storage"
	defaultNormalStatusLowerBoundStorage   = 0
	defaultNormalStatusUpperBoundStorage   = 50
	defaultWarningStatusLowerBoundStorage  = 50
	defaultWarningStatusUpperBoundStorage  = 80
	defaultCriticalStatusLowerBoundStorage = 80
	defaultCriticalStatusUpperBoundStorage = 100

	diskioStatusBound                     = "diskio"
	defaultNormalStatusLowerBoundDiskIO   = 0
	defaultNormalStatusUpperBoundDiskIO   = 50
	defaultWarningStatusLowerBoundDiskIO  = 50
	defaultWarningStatusUpperBoundDiskIO  = 80
	defaultCriticalStatusLowerBoundDiskIO = 80
	defaultCriticalStatusUpperBoundDiskIO = 100

	slaStatusBound                     = "sla"
	defaultNormalStatusLowerBoundSLA   = 98
	defaultNormalStatusUpperBoundSLA   = 100
	defaultWarningStatusLowerBoundSLA  = 80
	defaultWarningStatusUpperBoundSLA  = 98
	defaultCriticalStatusLowerBoundSLA = 0
	defaultCriticalStatusUpperBoundSLA = 80

	defaultStatusBound              = "default"
	defaultNormalStatusLowerBound   = 0
	defaultNormalStatusUpperBound   = 50
	defaultWarningStatusLowerBound  = 50
	defaultWarningStatusUpperBound  = 80
	defaultCriticalStatusLowerBound = 80
	defaultCriticalStatusUpperBound = 100

	defaultUptimeRatioTolerance = 0.9
)

// NewYamlConfigLoader return the YAML Configuration loader.
func NewYamlConfigLoader(fileLocation string) *YAMLConfigLoader {
	return &YAMLConfigLoader{
		fileLocation: fileLocation,
	}
}
