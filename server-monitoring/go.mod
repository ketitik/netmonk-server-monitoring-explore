module ketitik/netmonk/server-monitoring

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.4.2
	go.mongodb.org/mongo-driver v1.7.4
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)

replace bitbuket.org/ketitik/server-monitoring/domain => /home/fahrudin/play/src/bitbuket.org/ketitik/server-monitoring/domain
