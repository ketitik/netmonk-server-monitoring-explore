## How to run
```sh
go run main.go 
atau
go run /app/main.go

```

## How to test
```sh
curl localhost:8080/inventory/gateway
```

## Principle
- Separation of concern
- Independent of framework
- Independent of DB + External agency
- Testable
- Readable

## Folder Structure
```
.
├── app
│   └── main.go
├── config                          #dinamic config from config file
│   └── config.go
├── core
│   ├── entity
│   └── usecase                     #logic/usecase happen here
│       ├── inventory_usecase.go
│       └── monitoring_usecase.go
├── domain
│   ├── inventory.go                # model/entity here
│   └── monitoring.go
├── go.mod
├── go.sum
├── handler
│   ├── api                         # layer to generate response/delivery layer
│   │   └── handler.go
│   └── worker
│       └── handler.go
├── main.go
├── pkg                             # function helper for this service
└── repository                      # layer to provide data db, 3th party, another service
    ├── inventory_repo
    │   └── repository.go
    └── monitoring_repo
        └── repository.go
```