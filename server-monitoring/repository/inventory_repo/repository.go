package inventoryrepo

import (
	"fmt"

	inventory_intf "ketitik/netmonk/server-monitoring/domain"

	"go.mongodb.org/mongo-driver/mongo"
)

type repository struct {
	mongoDB *mongo.Database
}

func New(mongoDB *mongo.Database) inventory_intf.Inventory {
	return &repository{mongoDB: mongoDB}
}

func (r *repository) CheckGatewayExist(IPMgmt, InterfaceName string) (bool, error) {
	fmt.Println("implement me repo!")
	return false, nil
}

func (r *repository) GetAllCPUUsage(nodes map[string]inventory_intf.NodeInventory, pageConfig inventory_intf.PaginationConfig) ([]inventory_intf.CPUSummary, error) {
	fmt.Println("implement me")
	return nil, nil

}
