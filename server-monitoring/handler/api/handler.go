package apihandler

import (
	"fmt"
	"ketitik/netmonk/server-monitoring/core/usecase"

	"net/http"
)

type inventoryHandler struct {
	inventory_uc usecase.InventoryUsecase
}

// InventoryHandler list exposed HTTP handler
type InventoryHandler interface {
	CheckGatewayExist(w http.ResponseWriter, r *http.Request)
}

// NewInventoryHTTPHandler constructor
func NewInventoryHTTPHandler(inventory_uc usecase.InventoryUsecase) InventoryHandler {
	return &inventoryHandler{inventory_uc: inventory_uc}
}

func (h *inventoryHandler) CheckGatewayExist(w http.ResponseWriter, r *http.Request) {
	fmt.Println("implement me handler!")
	h.inventory_uc.CheckGatewayExist("a", "b")
	return
}
