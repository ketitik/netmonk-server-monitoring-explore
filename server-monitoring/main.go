package main

import (
	"net/http"

	"ketitik/netmonk/server-monitoring/core/usecase"
	apihandler "ketitik/netmonk/server-monitoring/handler/api"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	inventoryrepo "ketitik/netmonk/server-monitoring/repository/inventory_repo"
)

func main() {
	// ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("ATLAS_URI")))
	// if err != nil {
	// 	//panic(err)
	// }
	// defer client.Disconnect(ctx)
	invRepo := inventoryrepo.New(nil /*client.Database("test")*/)
	invUc := usecase.NewInventoryUsecase(invRepo)
	invHandler := apihandler.NewInventoryHTTPHandler(invUc)

	r := NewRouter(invHandler)

	// Run Web Server
	logrus.Printf("Starting http server at %v", "8080")
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		logrus.Fatalf("Unable to run http server: %v", err)
	}
	logrus.Println("Stopping API Service...")
}

// NewRouter returns router.
func NewRouter(invHandler apihandler.InventoryHandler) *mux.Router {
	r := mux.NewRouter()

	// Network Inventory
	r.HandleFunc("/inventory/gateway", invHandler.CheckGatewayExist).Methods("GET")

	return r
}
