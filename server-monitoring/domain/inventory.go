package domain

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// NodeInventory struct define the list of node inventory data
type NodeInventory struct {
	ID        bson.ObjectId       `bson:"_id" json:"id"`
	NodeName  string              `bson:"node_name" json:"node_name"`
	IPMgmt    string              `bson:"ip_mgmt" json:"ip_mgmt"`
	ProbeID   bson.ObjectId       `bson:"probe_id" json:"probe_id,omitempty"`
	ProbeName string              `bson:"probe_name" json:"probe_name,omitempty"`
	Tags      []string            `bson:"tags" json:"tags,omitempty"`
	Notes     []string            `bson:"notes" json:"notes,omitempty"`
	Threshold float64             `bson:"threshold" json:"threshold"`
	Modules   map[string][]string `bson:"modules" json:"modules"`
	Username  string              `bson:"username" json:"username,omitempty"`
	Password  string              `bson:"password" json:"password,omitempty"`
	CreatedAt time.Time           `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt time.Time           `bson:"updated_at" json:"updated_at,omitempty"`
}

//PaginationConfig is structure for pagination config
type PaginationConfig struct {
	Status      string
	Location    string
	Search      string
	Offset      int64
	Limit       int64
	Order       string
	By          string
	StatusBound map[string]StatusBound
}

// StatusBound defines lower bound and upper bound status
type StatusBound struct {
	LowerBound int `json:"lower_bound"`
	UpperBound int `json:"upper_bound"`
}

//CPUSummary is a structure for node's cpu information summary
type CPUSummary struct {
	Hostname          string  `json:"hostname"`
	NodeInventoryName string  `json:"node_inventory_name"`
	IPMgmt            string  `json:"ip_mgmt"`
	CPUIndex          string  `json:"cpu_index"`
	CPUUsage          float64 `json:"cpu_usage"`
	Location          string  `json:"location,omitempty"`
}

type Inventory interface {
	CheckGatewayExist(IPMgmt, InterfaceName string) (bool, error)
	GetAllCPUUsage(nodes map[string]NodeInventory, pageConfig PaginationConfig) ([]CPUSummary, error)
}
