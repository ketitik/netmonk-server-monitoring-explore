package usecase

import (
	"fmt"
	"ketitik/netmonk/server-monitoring/domain"
)

type InventoryUsecase interface {
	CheckGatewayExist(IPMgmt, InterfaceName string) (bool, error)
}

type inventoryUsecase struct {
	inventoryRepo domain.Inventory
}

func NewInventoryUsecase(inventoryRepo domain.Inventory) InventoryUsecase {
	return &inventoryUsecase{inventoryRepo: inventoryRepo}
}

func (uc *inventoryUsecase) CheckGatewayExist(IPMgmt, InterfaceName string) (bool, error) {
	fmt.Println("implement me usecase!")
	return uc.inventoryRepo.CheckGatewayExist(IPMgmt, InterfaceName)
}
