#rem ##########################################################################
#rem
#rem  telegraf instalation script for Windows
#rem  list command:
#rem    - telegraf.exe --service uninstall	Remove the telegraf service
#rem    - telegraf.exe --service start	    Start the telegraf service
#rem    - telegraf.exe --service stop	    Stop the telegraf service
#rem
#rem ##########################################################################

# Full path of the file
$file = 'C:\Program Files\InfluxData\telegraf\telegraf.exe'

function Is-Service-Exist {
    #check service exist
    try{
        $service = Get-Service -Name W32Time -ErrorAction SilentlyContinue
        return ($service.Length -gt 0) 
    }
    catch{
        return $false
    }
}

function IsServiceRunning {
    try {
        $ServiceName = 'telegraf'
        $arrService = Get-Service -Name $ServiceName
        $arrService.Status

        if ($arrService.Status -eq 'Running')
        {
            return $true
        }else{
            return $false
        }
    }
    catch{
        return $false
    }
}

$config = 'https://raw.githubusercontent.com/fahrudina/telegraf/main/telegraf.conf'
function Register-And-Start-Service {
    Write-Host "install Telegraf as a Windows service so that it starts automatically along with our system"  -ForegroundColor green
        C:\'Program Files'\InfluxData\telegraf\telegraf.exe --service install --config $config
    Write-Host "test once to check instalation work" -ForegroundColor green
        C:\'Program Files'\InfluxData\telegraf\telegraf.exe --config $config --test
    Write-Host "start telegraf as a service and collect data" -ForegroundColor green
        C:\'Program Files'\InfluxData\telegraf\telegraf.exe --config $config --service start

    Write-Host "Your Netmonk Agent is running and functioning properly. It will continue" -ForegroundColor green
    Write-Host "to run in the background and submit metrics to Netmonk." -ForegroundColor green
}


function Install-Telegraf {
    if (Test-Path -Path $file -PathType Leaf){
        if (IsServiceRunning){
            Write-Host "Stop telegraf."
            $ServiceName = 'telegraf'
            Stop-Service $ServiceName
        }
        
        if (Is-Service-Exist) {
            # if service allready exist in the system
            # uninstall service
            Write-Host "uninstall telegraf."
            C:\'Program Files'\InfluxData\telegraf\telegraf.exe --service uninstall
        }
        Write-Host "remove folder"
            Remove-Item -LiteralPath 'C:\Program Files\InfluxData\telegraf' -Force -Recurse -Confirm:$false -ErrorAction SilentlyContinue
    }

    Write-Host "download telegraf" 
        DownloadFile https://dl.influxdata.com/telegraf/releases/telegraf-1.20.2_windows_amd64.zip  telegraf-1.20.2_windows_amd64.zip
        #wget https://dl.influxdata.com/telegraf/releases/telegraf-1.20.2_windows_amd64.zip -UseBasicParsing -OutFile telegraf-1.20.2_windows_amd64.zip
    Write-Host "extract zip to C:\Program Files\InfluxData\telegraf\" 
        Expand-Archive .\telegraf-1.20.2_windows_amd64.zip -DestinationPath 'C:\Program Files\InfluxData\telegraf\'
    Write-Host "move telegraf binary and config to parent directory" 
        mv 'C:\Program Files\InfluxData\telegraf\telegraf-1.20.2\telegraf.*' 'C:\Program Files\InfluxData\telegraf'
    
    Register-And-Start-Service
}

function Question-To-Install {
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription '&Yes', 'Install ulang'
    $no = New-Object System.Management.Automation.Host.ChoiceDescription '&No', 'Tidak install ulang'
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
    $result = $host.ui.PromptForChoice('Install ulang?', 'Apakah anda ingin install?', $options, 0)

    switch ($result)
        {
            0 {
                $message = "Melakukan install ..."
                $install = $true
            }
            1 {
                $install = $false
            }
        }
    Write-Host $message
    return $install
}


function DownloadFile($url, $targetFile)
{

   $uri = New-Object "System.Uri" "$url"
   $request = [System.Net.HttpWebRequest]::Create($uri)
   $request.set_Timeout(15000) #15 second timeout
   $response = $request.GetResponse()
   $totalLength = [System.Math]::Floor($response.get_ContentLength()/1024)
   $responseStream = $response.GetResponseStream()
   $targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $targetFile, Create
   $buffer = new-object byte[] 10KB
   $count = $responseStream.Read($buffer,0,$buffer.length)
   $downloadedBytes = $count

   while ($count -gt 0)
   {
       $targetStream.Write($buffer, 0, $count)
       $count = $responseStream.Read($buffer,0,$buffer.length)
       $downloadedBytes = $downloadedBytes + $count
       Write-Progress -activity "Downloading file '$($url.split('/') | Select -Last 1)'" -status "Downloaded ($([System.Math]::Floor($downloadedBytes/1024))K of $($totalLength)K): " -PercentComplete ((([System.Math]::Floor($downloadedBytes/1024)) / $totalLength)  * 100)
   }

   Write-Progress -activity "Finished downloading file '$($url.split('/') | Select -Last 1)'"
   $targetStream.Flush()
   $targetStream.Close()
   $targetStream.Dispose()
   $responseStream.Dispose()

}

#If the file exist, ask what action need to do
if (Test-Path -Path $file -PathType Leaf) {
    Write-Host "Telegraf sudah terinstall, apakah kamu mau install ulang?"
    if (Question-To-Install) {
        Install-Telegraf
    }else {
        #check service exist
        if (-not(Is-Service-Exist)) {
            # Service not exist in system try to register service to system and start it
            Register-And-Start-Service
        }
    }
}else {
    #Telegraf not exist, start to install telegraf
    Install-Telegraf
}